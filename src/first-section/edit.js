/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */

import { useBlockProps, InnerBlocks } from '@wordpress/block-editor'


/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 * 
 * @return {WPElement} Element to render.
 */
export default function Edit({ attributes, setAttributes }) {  
	const blockProps = useBlockProps()

	// Template de blocs
	const BASE_TEMPLATE = 
	[ 
		[ 'core/columns', {className: "aligndefault"}, 
			[
				[ 'core/column', { templateLock: 'all' }, 
					[
						[ 'core/heading',{placeholder:"Titre page de niveau 1", level:1} ],
						[ 'core/paragraph',{placeholder:"Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte."} ],
					] 
				],
				[ 'core/column', {}, [] 
				],
			] 
		] 
	];

	return (

    		<div { ...blockProps }>
				<InnerBlocks
					template={ BASE_TEMPLATE } // Le template de base
				/>
			</div>
	)
}
