<?php
/**
 * Plugin Name:       Effidic
 * Description:       block gutenberg custom pour effidic
 * Requires at least: 5.8
 * Requires PHP:      7.1
 * Version:           0.1.0
 * Author:            I Have a Green
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       effidic
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function create_block_effidic_block_init() {
	register_block_type( __DIR__ . '/build/card' );
	register_block_type( __DIR__ . '/build/card-btn' );
	register_block_type( __DIR__ . '/build/first-section' );
	register_block_type( __DIR__ . '/build/section-bg' );
	register_block_type( __DIR__ . '/build/job-card' );
	register_block_type( __DIR__ . '/build/number' );
	register_block_type( __DIR__ . '/build/newsletter' );
    register_block_type( __DIR__ . '/build/img-custom' );
    register_block_type( __DIR__ . '/build/section-methodo-child' );
    register_block_type( __DIR__ . '/build/section-methodo' );
}
add_action( 'init', 'create_block_effidic_block_init' );


